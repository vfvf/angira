.. angira documentation master file, created by
   sphinx-quickstart on Mon Jun 17 22:16:58 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
   
.. rst-class:: hide-header

Angira
========

.. image:: _static/angira_logo.png
   :alt: Angira data validation and semantic typing 
   :align: center 


Angira is a collection of tools for semantic type checking, concept learning, data validation and automated data curation on semi-structured data files/tables. 
It supports table extraction from messy excel files, and generating concepts/semantic type categories to automate machine understanding of data.   



Installing
----------

Install by git pulling the package and running 

.. code-block:: text
    
    python setup.py install -f  


Example use are in jupyter notebook
------------------------------------

 .. code-block:: python 
     
     import angira as angira
     # Example of value validation 

`EXAMPLE jupyter <_static/Angira_use_case.html>`_
     
Semantic Types are defined by the following json structure. The structure is flexible enough: several functions only require a couple of the key words 
to be present to work. Each semantic-type is a dictionary, and the collection of Semantic Types is a dictionary keyed by Semantic Type name.
All function that expects 'config' expects a dictionary of such a collection (or singleton dict.). In the example below, all entries with angular brackets
and place-holders. Define your Sementic type (remove all angular brackets obviously). 

.. code-block:: python
   {
	<semantic type name>: 
       {"description" : < some free text describing the type for NLP (optional) > 
        "defaultSearchFx" : [(<token>, <search_function_name>, <dict of params>, <error_code>, <weight>), ...] # list of search function definitions
        #available in Searcher module for searching for the name of the data entry. Each element in the list is a tuple, 
        #with token to search by, say, 'ISIS' for ASO number,
        #the search function, say, 're.match' (as a string), parameter dict. to pass to search function, the error-code for the search, weight  
        # (weight will be used in future, provide default value of 1.0)  
        "valueValidators": [(<fucntname_in_validators>, < dict of params>, <error-code> , <weight>  ),...] #list of validators for the entries/values
        #of the column/vector of data semantic type. Each entry is a tuple of a validator that will be composed for that column. Typical 
        #functions are found in validators module. 
        "concepts" : [<concept associated with this data> ] #OPTIONAL. This is going to be used in future for concept learning 
        "searchFx": [(),..] #optional search function to OVERRIDE the default behavior, say your column name is really odd and you want to 
        # modify the behavior locally.. 
       }, 
       ....
    }	

Contents
================

.. toctree::
   :maxdepth: 5
   :caption: Contents:

Angira
=================

.. automodule:: angira.angira
   :members:

Validators
=================
.. automodule:: angira.validators.validators
   :members:

Searchers
=================
.. automodule:: angira.searchers.searchers
   :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
